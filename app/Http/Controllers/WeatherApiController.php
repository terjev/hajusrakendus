<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class WeatherApiController extends Controller
{
    public function index()
    {
        $url = 'https://api.openweathermap.org/data/2.5/weather?q=kuressaare&units=metric&appid=045ba0bd18d3871b214b201cf226cd75';
        $data = $this->cacheData($url);
        return Inertia::render('Weather', [
            'data' =>$data
        ]);
    }
    private function cacheData($url)
    {
        if (!Cache::has('weather')) {
            $query = Http::get($url)->json();
            Cache::put('weather',$query, now()->addHour());
        }
        return Cache::get('weather');
    }
}
